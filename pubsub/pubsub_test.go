package pubsub

import (
	"fmt"
	"testing"
)

/**** Init ****/

/**** Simple test ****/

func hello(ev *Event) {
	fmt.Println("hello", ev)
}

func TestSimple(t *testing.T) {
	var ev_test_event EventType = 1
	Init()
	Subscribe(ev_test_event, hello)
	Publish(ev_test_event, "data")
}

/**** Method test ****/

type S struct {
	v int
}

func (s *S) hello(ev *Event) {
	fmt.Println("hello", ev)
	s.v = 2
	println(s.v)
	fmt.Printf("p %p\n", s)
}

func TestMethod(t *testing.T) {
	var ev_test_method EventType = 2
	s := S{v: 1}
	println(s.v)
	Subscribe(ev_test_method, s.hello)
	Publish(ev_test_method, "data")
	println(s.v)
	fmt.Printf("p %p\n", &s)
}

/**** Test multiple ****/

func hello1(ev *Event) {
	fmt.Println("hello1", ev)
}

func hello2(ev *Event) {
	fmt.Println("hello2", ev)
}

func TestMultiple(t *testing.T) {
	var ev_test_multiple EventType = 3
	Init()
	Subscribe(ev_test_multiple, hello1)
	Subscribe(ev_test_multiple, hello2)
	Publish(ev_test_multiple, "data")
}

/**** Test multiple string version ****/

func hello1S(ev *EventS) {
	fmt.Println("S hello1", ev)
}

func hello2S(ev *EventS) {
	fmt.Println("S hello2", ev)
}

func TestMultipleSV(t *testing.T) {
	var evname = "ev_test_multiple_sv"
	Init()
	SubscribeS(evname, hello1S)
	SubscribeS(evname, hello2S)
	//SubscribeS("_", hello2S)
	PublishS(evname, "data")
}

/**** Test many args ****/

func helloMA(ev *EventS) {
	fmt.Println("helloMA", ev)
}

func TestManyArgs(t *testing.T) {
	var evname = "ev_test_many_args"
	Init()
	SubscribeS(evname, hello1S)
	//SubscribeS("_", hello2S)
	PublishS(evname, "data", 1, 2)
	PublishS(evname, "data", 1)
}

