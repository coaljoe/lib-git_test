package pubsub

import (
	"fmt"
)

type EventType int

type Event struct {
	Type EventType
	Data interface{}
}

type EventS struct {
	Name string
	Data []interface{}
}

type Callback func(ev *Event)
type CallbackS func(ev *EventS)

// XXX fixme: should have multiple callbacks?
var psmap map[EventType][]Callback
var psmapS map[string][]CallbackS // String version
var initialized bool

func Init() {
	if !initialized {
		fmt.Println("pubsub init")
		psmap = make(map[EventType][]Callback)
		psmapS = make(map[string][]CallbackS)
		initialized = true
	}
}

func Publish(evtype EventType, data interface{}) {
	Init()
	for _, fp := range psmap[evtype] {
		fp(&Event{evtype, data})
	}
}

func Subscribe(evtype EventType, fp Callback) {
	Init()
	a := psmap[evtype]
	if a == nil {
		a = make([]Callback, 0)
	}
	a = append(a, fp)
	psmap[evtype] = a
	//fmt.Println("psmap", psmap)
	//fmt.Println("a", a)
}

func Unsubscribe(evtype EventType, fp Callback) {
	delete(psmap, evtype)
}
/*
func Update() {
  for evtype, fn := range psmap {
  }
}
*/

// String versions
func PublishS(evtype string, data ...interface{}) {
	Init()
	for _, fp := range psmapS[evtype] {
		fp(&EventS{evtype, data})
	}
}

func SubscribeS(evtype string, fp CallbackS) {
	Init()
	a := psmapS[evtype]
	if a == nil {
		a = make([]CallbackS, 0)
	}
	a = append(a, fp)
	psmapS[evtype] = a
	//fmt.Println("psmap", psmap)
	//fmt.Println("a", a)
}

func UnsubscribeS(evtype string, fp CallbackS) {
	delete(psmapS, evtype)
}