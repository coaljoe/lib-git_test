package ecs

import "fmt"

type World struct {
	entities map[int]*Entity
	// XXX entities with multiple same objects not handling properly?
	// not tested. can use tags for it(?)
	componentEntityMap map[interface{}]*Entity
}

func NewWorld() *World {
	w := &World{}
	w.entities = make(map[int]*Entity)
	w.componentEntityMap = make(map[interface{}]*Entity)
	return w
}

func (w *World) CreateEntity() *Entity {
	en := newEntity()
	w.AddEntity(en)
	return en
}

func (w *World) AddEntity(en *Entity) {
	if w.HasEntity(en) {
		panic(fmt.Sprintf("wolrd already have this entity. id=", en.id))
	}
	w.entities[en.id] = en
}

func (w *World) RemoveEntity(en *Entity) {
	if !w.HasEntity(en) {
		panic(fmt.Sprintf("can't remove entity, not found. id=", en.id))
	}
	delete(w.entities, en.id)
}

func (w *World) HasEntity(en *Entity) bool {
	_, ok := w.entities[en.id]
	return ok
}

func (w *World) GetEntityById(id int) *Entity {
	return w.entities[id]
}