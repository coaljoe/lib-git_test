package ecs

import (
	"fmt"
	"github.com/stretchr/testify/assert"
	"bitbucket.org/coaljoe/lib/sr"
	"reflect"
	"testing"
)

func (c *C1) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		c.name,
	), nil
}

func (c *C1) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&c.name,
	)
	return nil
}

func (c *C2) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		c.name,
	), nil
}

func (c *C2) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&c.name,
	)
	return nil
}

func (c *C3) GobEncode() ([]byte, error) {
	return sr.SerializeByFields(
		c.name,
	), nil
}

func (c *C3) GobDecode(b []byte) error {
	sr.DeserializeByFields(b,
		&c.name,
	)
	return nil
}

func TestSerializeComponent(t *testing.T) {
	//var c1, c2 *Component

	sr.Register(&C1{})
	sr.Register(&C2{})
	sr.Register(&C3{})

	e1 := NewEntity()
	//_ = newC1(e1, "test c1")
	//_ = newC2(e1, "test c2")
	//_ = newC3(e1, "test c3")
	e1.AddComponent(newC1("test c1"))
	e1.AddComponent(newC2("test c2"))

	c1 := e1.Get(C1T).(*C1)
	//c2 = NewNode(NodeType_Mesh)
	c2 := &C1{}
	//c2.setDefaults()

	b := sr.SerializeValue(c1)
	_ = b
	sr.DeserializeValue(b, c2)
	fmt.Printf("c1: %#v\n\n", c1)
	fmt.Printf("c2: %#v\n\n", c2)

	//c1.entity = nil
	//c2.entity = nil

	if !reflect.DeepEqual(c1, c2) {
		t.Log("Not equal (fine)")
		t.Fail()
		//panic(2)
	}

	e2 := &Entity{}
	fmt.Printf("e2: %#v\n\n", e2)

	b = sr.SerializeValue(e1)
	sr.DeserializeValue(b, e2)
	fmt.Printf("e1: %#v\n\n", e1)
	fmt.Printf("e2: %#v\n\n", e2)

	if !reflect.DeepEqual(e1, e2) {
		t.Log("Not equal")
		t.Fail()
		//panic(2)
	}

	// Verify
	assert.Equal(t, e1.id, e2.id)
	//assert.Equal(t, e1.components, e2.components)
	if !reflect.DeepEqual(e1.components, e2.components) {
		t.Fail()
	}
	assert.Equal(t, e1.tagsmap, e2.tagsmap)

	c1 = e2.Get(C1T).(*C1)
	//c1 = e2.Get(&C1{}).(*C1)
	//println(c1.entity)
	e1.PrintComponents()
	e2.PrintComponents()
	for _, c := range e1.Components() {
		fmt.Printf("%T (%p)\n", c, c)
	}
	for _, c := range e2.Components() {
		fmt.Printf("%T (%p)\n", c, c)
	}
	println(GetEntity(c1))

	println("derp")

}
