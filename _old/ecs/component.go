package ecs

import "fmt"

//type Component interface{}

type DeleteComponentI interface {
	// XXX rename to OnDeleteComponent?
	DeleteComponent()
}

type ComponentI interface {
	SetEntityId(id int)
	GetEntityId() int
	GetEntity() *Entity
}

type Component struct {
	EntityId int
}

func NewComponent() *Component {
	c := &Component{
		EntityId: -1, // not set
	}
	return c
}

func (c *Component) SetEntityId(id int) {
	c.EntityId = id
}

func (c *Component) GetEntityId() int {
	return c.EntityId
}

// XXX fixme: doesn't work with custom worlds(?)
// not tested
func (c *Component) GetEntity() *Entity {
	println("ecs: Component: GetEntity")
	println("defaultWorld entities:")
	fmt.Printf("%#v\n", defaultWorld.entities)
	println(len(defaultWorld.entities))
	return defaultWorld.entities[c.EntityId]
}

// Never subclass Component

var _ = `

import "reflect"


type DeleteComponentI interface {
	DeleteComponent()
}

type HasKindI interface {
	Kind() reflect.Type
}

type ComponentI interface {
	setEntity(en *Entity)
}

type Component struct {
	entity *Entity // backlink to entity
	derp   bool
	//kind   reflect.Type
	//data   interface{}
}

func NewComponent(en *Entity, cmp interface{}) *Component {
	c := &Component{
		entity: en,
		derp:   true,
		//kind:   reflect.TypeOf(cmp),
	}
	//fmt.Println("c=%v", c.kind)
	// auto register
	en.AddComponent(cmp)
	return c
}

func (c *Component) setEntity(en *Entity) { c.entity = en }

/*
func (c *Component) Kind() reflect.Type {
	return c.kind
}
*/

// Shortcut to Entity.GetComponent.
func (c *Component) GetComponent(cmp interface{}) interface{} {
	return c.entity._getComponent(cmp, false)
}

// Alias for GetComponent.
func (c *Component) Get(cmp interface{}) interface{} {
	return c.GetComponent(cmp)
}

// Get Component's entity.
func (c *Component) GetEntity() *Entity {
	if c == nil {
		panic("component is nil")
	}
	return c.entity
}

// Delete for cleanup. fixme?
func (c *Component) DeleteComponent() {
	println("ecs.Component.DeleteComponent [override]")
	//c.entity = nil // gives nil errors?
}
`
