package ecs

import (
	"fmt"
	//"math"
	//"math/rand"
	"reflect"
	"strconv"
)

// XXX not persistent?
// use uniq (random) id?
var maxEntId int

type Typeable interface {
	Type() string
}

type componentRecord struct {
	kind reflect.Type
	//data interface{} // XXX not used
}

type Entity struct {
	// Read-only, never reuse
	id         int
	components []interface{}
	// Same size as components
	componentRecords []componentRecord
	// XXX not fully implemented
	//componentsMap    map[reflect.Type]interface{}
	// XXX not fully implemented
	//componentsMap2 map[string]interface{}
	tagsmap map[string]interface{}
}

func (en *Entity) Id() int                   { return en.id }
func (en *Entity) Components() []interface{} { return en.components }

func NewEntity() *Entity {
	//return defaultWorld.CreateEntity()
	en := defaultWorld.CreateEntity()
	println(len(defaultWorld.entities))
	panic(2)
	return en
}

func newEntity() *Entity {
	maxEntId += 1
	return &Entity{
		id: maxEntId - 1,
		//id: rand.Intn(math.MaxInt64),
		componentRecords: make([]componentRecord, 0),
		//componentsMap:    make(map[reflect.Type]interface{}),
		//componentsMap2:   make(map[string]interface{}),
		tagsmap: make(map[string]interface{}),
	}
}

func (en *Entity) OnSerialize() {
	println("ecs: Entity: OnSerialize")
	// XXX nothing
}

func (en *Entity) OnDeserialize() {
	println("ecs: Entity: OnDeserialize")
	//fmt.Printf("en: %#v\n\n", en)
	//fmt.Println("x: ", len(en.components))

	/*
		// Restore backlinks
		for _, ci := range en.components {
			//fmt.Printf("ci: %#v\n\n", ci)
			ci.(ComponentI).setEntity(en)
		}
	*/

	// Restore componentRecords
	en.componentRecords = make([]componentRecord, 0)
	for _, ci := range en.components {
		cmp := ci
		en.componentRecords = append(en.componentRecords,
			componentRecord{kind: reflect.TypeOf(cmp)})
	}

	//showComponentEntityMap()

	// Re-add components information to defaultWorld
	// XXX TODO: should remove old references somehow(?)
	for _, ci := range en.components {
		defaultWorld.componentEntityMap[ci] = en
	}

	//showComponentEntityMap()
}

// Add component.
func (en *Entity) AddComponent(cmp interface{}) {
	// check identical components
	for _, cmp1 := range en.components {
		if cmp1 == cmp {
			panic("identical component already added")
		}
	}
	en.components = append(en.components, cmp)
	en.componentRecords = append(en.componentRecords,
		componentRecord{kind: reflect.TypeOf(cmp)})
	//en.componentsMap[reflect.TypeOf(cmp)] = cmp
	//en.componentsMap2[cmp.(Typeable).Type()] = cmp

	defaultWorld.componentEntityMap[cmp] = en

	cmp.(ComponentI).SetEntityId(en.id)
}

// Add component with tag.
func (en *Entity) AddComponentTag(cmp interface{}, tag string) {
	en.AddComponent(cmp)
	en.tagsmap[tag] = cmp
}

func (en *Entity) _getComponent(cmp interface{}, silent bool) interface{} {
	kind := reflect.TypeOf(cmp)
	/*
		var kind reflect.Type
		if v, ok := cmp.(HasKindI); ok {
			kind = v.Kind()
			fmt.Println("kind=%v", kind)
		} else {
			fmt.Println("v=%v", v)
			panic(2)
		}
	*/
	for i, cmp1 := range en.components {
		//if reflect.TypeOf(cmp1) == kind {
		//if cmp1.Kind() == kind {
		if en.componentRecords[i].kind == kind {
			return cmp1
		}
	}
	if !silent {
		//println("*component not found")
		// panic by default
		println("*component not found; type: " + kind.String())
		println("*list of components")
		en.PrintComponents()
		panic("ecs.error")
	}
	return nil
}

func _makeComponentTag(cmp interface{}, tag string) string {
	_tag := tag + "^" + reflect.TypeOf(cmp).Elem().Name()
	return _tag
}

func (en *Entity) GetComponent(cmp interface{}) interface{} {
	return en._getComponent(cmp, false)
}

func (en *Entity) GetComponentCheck(cmp interface{}) (c interface{}, ok bool) {
	ok = false
	c = en._getComponent(cmp, true)
	if !(c == nil || reflect.ValueOf(c).IsNil()) {
		ok = true
	}
	return c, ok
}

// Get component with type, specified by tag.
func (en *Entity) GetComponentTag(cmp interface{}, tag string) interface{} {
	// extra type checks? fixme
	kind := reflect.TypeOf(cmp)
	for _, cmp1 := range en.components {
		if reflect.TypeOf(cmp1) == kind {
			_tag := _makeComponentTag(cmp, tag)
			if c, ok := en.tagsmap[_tag]; ok {
				return c
			}
			//return en.GetComponentTagOnly(tag)
		}
	}
	println("*component not found; type: " + kind.String() + " tag: " + tag)
	// panic by default
	println("*list of components")
	en.PrintComponents()
	panic("ecs.error")
	return nil
}

/*
// XXX Not fully imlemented.
// Slighthly slower than standard version in benchmark.
func (en *Entity) _GetFast(cmpType reflect.Type) interface{} {
	return en.componentsMap[cmpType]
}

func (en *Entity) _GetFast2(cmpType Typeable) interface{} {
	return en.componentsMap2[cmpType.Type()]
}
*/

/*
func (en *Entity) GetComponentTagOnly(tag string) interface{} {
	if c, ok := en.tagsmap[tag]; ok {
		return c
	}
	println("*component not found; tag: " + tag)
	return nil
}
*/

// Alias for GetComponent.
func (en *Entity) Get(cmp interface{}) interface{} {
	return en.GetComponent(cmp)
}

// Set tag for a component.
func (en *Entity) SetComponentTag(cmp interface{}, tag string) {
	for _, c := range en.components {
		if reflect.DeepEqual(cmp, c) {
			_tag := _makeComponentTag(cmp, tag)
			en.tagsmap[_tag] = c
			/*
			   Println(en.tagsmap)
			   for k, v := range en.tagsmap {
			     Println("k", k, "v", v)
			   }
			*/
			return
		}
	}
	println("*component not found; tag: " + tag)
	panic(1)
}

func (en *Entity) HasComponent(cmp interface{}) bool {
	return en._getComponent(cmp, true) != nil
}

func (en *Entity) ListComponents() string {
	if en == nil {
		panic("entity is nil")
	}
	//fmt.Println(en.components)
	str := ""
	for _, c := range en.components {
		kind := reflect.TypeOf(c)
		//str += kind.String()
		//fmt.Println("kind:", kind)
		str += kind.Elem().Name() // fixme: works only with pointer components(?)
		for tag, c1 := range en.tagsmap {
			if reflect.TypeOf(c1) == kind {
				str += " [tag: " + tag + "]"
			}
		}
		str += " "
	}
	return str
}

func (en *Entity) PrintComponents() {
	if en == nil {
		panic("entity is nil")
	}
	fmt.Printf("En.PrintComponents begin (id=%s) \n", strconv.Itoa(en.id))
	fmt.Println(" " + en.ListComponents())
	fmt.Println("end")
	fmt.Println("components len:", len(en.components))
}

func (en *Entity) removeComponentIdx(i int) {
	// get cmp
	cmp := en.components[i]

	// remove from tags
	for tag, c1 := range en.tagsmap {
		if reflect.DeepEqual(cmp, c1) {
			delete(en.tagsmap, tag)
		}
	}

	// remove from components
	en.components = append(en.components[:i], en.components[i+1:]...)

	// Remove from componentRecords
	en.componentRecords = append(en.componentRecords[:i],
		en.componentRecords[i+1:]...)

	// call component destroyer. fixme?
	if v, ok := cmp.(DeleteComponentI); ok {
		v.DeleteComponent()
	}
}

func (en *Entity) RemoveAllComponents() bool {
	c_len := len(en.components)
	for i := 0; i < c_len; i++ {
		en.removeComponentIdx(0)
	}
	// check
	if len(en.components) > 0 || len(en.tagsmap) > 0 {
		panic("RemoveAllComponents check error")
	}
	return true
}

func (en *Entity) String() string {
	return fmt.Sprintf("<Entity id=%d>", en.id)
}
