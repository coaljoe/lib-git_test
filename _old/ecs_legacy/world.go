package ecs

import "fmt"

type World struct {
	entities map[int]*Entity
}

func NewWorld() *World {
	w := &World{}
	w.entities = make(map[int]*Entity)
	return w
}

func (w *World) CreateEntity() *Entity {
	en := NewEntity()
	w.AddEntity(en)
	return en
}

func (w *World) AddEntity(en *Entity) {
	if w.HasEntity(en) {
		panic(fmt.Sprintf("wolrd already have this entity. id=", en.id))
	}
	w.entities[en.id] = en
}

func (w *World) RemoveEntity(en *Entity) {
	if !w.HasEntity(en) {
		panic(fmt.Sprintf("can't remove entity, not found. id=", en.id))
	}
	delete(w.entities, en.id)
}

func (w *World) HasEntity(en *Entity) bool {
	_, ok := w.entities[en.id]
	return ok
}
