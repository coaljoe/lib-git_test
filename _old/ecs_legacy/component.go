package ecs

import "reflect"

type DeleteComponentI interface {
	DeleteComponent()
}

type HasKindI interface {
	Kind() reflect.Type
}

type ComponentI interface {
	setEntity(en *Entity)
}

type Component struct {
	entity *Entity // backlink to entity
	derp   bool
	//kind   reflect.Type
	//data   interface{}
}

func NewComponent(en *Entity, cmp interface{}) *Component {
	c := &Component{
		entity: en,
		derp:   true,
		//kind:   reflect.TypeOf(cmp),
	}
	//fmt.Println("c=%v", c.kind)
	// auto register
	en.AddComponent(cmp)
	return c
}

func (c *Component) setEntity(en *Entity) { c.entity = en }

/*
func (c *Component) Kind() reflect.Type {
	return c.kind
}
*/

// Shortcut to Entity.GetComponent.
func (c *Component) GetComponent(cmp interface{}) interface{} {
	return c.entity._getComponent(cmp, false)
}

// Alias for GetComponent.
func (c *Component) Get(cmp interface{}) interface{} {
	return c.GetComponent(cmp)
}

// Get Component's entity.
func (c *Component) GetEntity() *Entity {
	if c == nil {
		panic("component is nil")
	}
	return c.entity
}

// Delete for cleanup. fixme?
func (c *Component) DeleteComponent() {
	println("ecs.Component.DeleteComponent [override]")
	//c.entity = nil // gives nil errors?
}
