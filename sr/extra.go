package sr

import (
	"encoding/gob"
)

// Force encoding decoding type
var forceEncodeTypeJson = false

// Force encode/decode type
func ForceEncodeTypeJson(v bool) {
	forceEncodeTypeJson = v
}


// XXX hack/buggy, use with caution
// Gob to json enc. encode data via  gob
func GobJsonEnc(g gob.GobEncoder) ([]byte, error) {
	ForceEncodeTypeJson(true)
	b, err := g.GobEncode()
	ForceEncodeTypeJson(false)

	return b, err
}

func GobJsonDec(g gob.GobDecoder, b []byte) error {
	ForceEncodeTypeJson(true)
	g.GobDecode(b)
	ForceEncodeTypeJson(false)

	return nil
}
