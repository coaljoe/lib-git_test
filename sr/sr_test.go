package sr

import (
	"fmt"
	"testing"
	//"encoding/gob"
)

func TestSr(t *testing.T) {
	var b []byte

	//s := "test string"
	s := []float64{1, 2, 3.}
	b = SerializeValue(s)

	xb := string(b)
	fmt.Println("b:", b)
	fmt.Println("xb:", xb)

	type Child struct {
		V int
	}

	type Base struct {
		X int
		z float64
		C *Child
	}

	ob := &Base{
		X: 10,
		//z: 1.3,
		C: &Child{V: 1},
	}
	b = SerializeValue(ob)

	fmt.Println("b:", b)
	fmt.Println("s b:", string(b))

	b = SerializeByFields(
		&ob.X, &ob.z)

	fmt.Println("b:", b)
	fmt.Println("s b:", string(b))

	// XXX

	type ZLevel int

	var l0 ZLevel
	l0 = 11

	b = SerializeValue(l0)


	type B2 struct {
		X int
		l1 ZLevel
	}

	b2 := &B2{}

	//Register(ZLevel{})
	Register(ZLevel(0))
	//gob.Register(ZLevel{})
	//gob.Register(ZLevel(0))

	b = SerializeByFields(
		&b2.X, &b2.l1)
}
