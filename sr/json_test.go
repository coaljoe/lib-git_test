package sr

import (
	"fmt"
	"testing"
)

func TestJson(t *testing.T) {
	var b []byte

	//s := "test string"
	s := []float64{1, 2, 3.}
	b = SerializeValueJson(s)

	xb := string(b)
	fmt.Println("b:", b)
	fmt.Println("xb:", xb)

	type Child struct {
		V int
	}

	type Base struct {
		X int
		z float64
		C *Child
	}

	ob := &Base{
		X: 10,
		//z: 1.3,
		C: &Child{V: 1},
	}
	b = SerializeValueJson(ob)

	fmt.Println("b:", b)
	fmt.Println("s b:", string(b))

	b = SerializeByFieldsJson(
		&ob.X, &ob.z)

	fmt.Println("b:", b)
	fmt.Println("s b:", string(b))
}
