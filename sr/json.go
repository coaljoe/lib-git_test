package sr

import (
	"bytes"
	"encoding/json"
)

// Attempt to serialize whole value.
func SerializeValueJson(v interface{}) []byte {
	return SerializeValueType(true, v)
}

func DeserializeValueJson(b []byte, v interface{}) {
	DeserializeValueType(true, b, v)
}

func SerializeByFieldsJson(fv ...interface{}) []byte {
	return SerializeByFieldsType(true, fv...)
}

func SerializeByFieldsKVJson(obj interface{}, fv ...interface{}) []byte {
	return SerializeByFieldsKVType(true, obj, fv...)
}

func DeserializeByFieldsJson(b []byte, fv ...interface{}) {
	DeserializeByFieldsType(true, b, fv...)
}

func DeserializeByFieldsKVJson(b []byte, fv ...interface{}) {
	DeserializeByFieldsType(true, b, fv...)
}

func _jsonNewEnc() (*bytes.Buffer, *json.Encoder) {
	w := new(bytes.Buffer)
	enc := json.NewEncoder(w)
	return w, enc
}

func _jsonNewDec(buf []byte) *json.Decoder {
	r := bytes.NewBuffer(buf)
	dec := json.NewDecoder(r)
	return dec
}

func _jsonEnc(enc *json.Encoder, v interface{}) {
	err := enc.Encode(v)
	if err != nil {
		panic(err)
	}
}

func _jsonDec(dec *json.Decoder, v interface{}) error {
	err := dec.Decode(v)
	if err != nil {
		//panic(err)
	}
	return err
}

func _jsonEncKV(enc *json.Encoder, k string, v interface{}) {
	m := make(map[string]interface{})
	m[k] = v
	err := enc.Encode(m)
	if err != nil {
		panic(err)
	}
}
