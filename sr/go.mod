module hg.sr.ht/~coaljoe/lib/sr

go 1.17

require (
	github.com/WastedCode/serializer v0.0.0-20150605061548-b76508a5f9d4
	github.com/iancoleman/orderedmap v0.2.0
)
