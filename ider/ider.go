package ider

import (
	"fmt"
	v4 "github.com/fossoreslp/go-uuid-v4"
)

type UUID = v4.UUID

type Ider struct {
	idmap map[string]int
}

func NewIder() *Ider {
	return &Ider{
		idmap: make(map[string]int, 0),
	}
}

func (i *Ider) Idmap() map[string]int { return i.idmap }

func (i *Ider) HasId(name string) bool {
	_, has := i.idmap[name]
	return has
}

func (i *Ider) SetId(name string, id int) {
	i.idmap[name] = id
}

// Get current id.
func (i *Ider) GetId(name string) int {
	if !i.HasId(name) {
		panic("ider: no such id; name: " + name)
	}
	return i.idmap[name]
}

func (i *Ider) GetNextId(name string) int {
	if !i.HasId(name) {
		i.idmap[name] = -1
		fmt.Println("ider: create id, name:", name)
	}
	//i.idmap[name]++
	i.idmap[name] = i.idmap[name] + 1
	return i.idmap[name]
}

func (i *Ider) GetUUID() UUID {
	return GetUUID()
}

func GetUUID() UUID {
	v4UUID, err := v4.New()
	if err != nil {
		panic(nil)
	}
	//fmt.Println(v4UUID)
	return v4UUID
}
