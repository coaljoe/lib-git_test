package ider

import (
	"fmt"
	"testing"
)

func TestIder(t *testing.T) {
	ider := NewIder()

	var id int
	/*
		id = ider.GetId("test")
		if id != 0 {
			t.Fatal(id)
		}
	*/

	id = ider.GetNextId("test")
	if id != 0 {
		t.Fatal(id)
	}

	id = ider.GetNextId("test")
	if id != 1 {
		t.Fatal(id)
	}

	id = ider.GetNextId("test1")
	if id != 0 {
		t.Fatal(id)
	}

	ider.SetId("test1", 10)
	id = ider.GetNextId("test1")
	if id != 11 {
		t.Fatal(id)
	}
}

func TestIderUUID(t *testing.T) {
	ider := NewIder()

	uuid := ider.GetUUID()
	fmt.Println(uuid)

	// Test global
	uuid2 := GetUUID()
	fmt.Println("uuid2:", uuid2)

	if uuid == uuid2 {
		t.Fatal()
	}
}
