package xlog

import "testing"

func TestLogger(t *testing.T) {
	l := NewLogger("test", LogDebug)
	l.SetWriteFuncName(true)
	l.SetWriteFilename(false)
	//l.SetWriteTime(true)
	l.Inf("test")
	l.Inf("[XSubsys] test", 1, 2, 3)
	l.EnableFilterPrefix("[XSubsys]")
	//l.EnableFilterPrefix("XSubsys]")
	l.Inf("[XSubsys] test", 3, 2, 1)
	//l.DisableFilterPrefix("[XSubsys]")
	//l.ClearFilterPrefixes()
	l.Inf("[XSubsys] test", 3, 3, 3)
	l.Inf("f=%F \nnew line", 3, 3, 3)
	l.Inf("f=%F \nnew line2", "\n test 1:", 1, "\n test 2:", 2, "\n test3:", "3a")

	l.ToggleEnabled()
	l.Inf("not visible output")
	l.ToggleEnabled()
	l.Inf("visible output")
}
