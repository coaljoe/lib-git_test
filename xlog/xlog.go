package xlog

import (
	"fmt"
	"io"
	//"log"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"
)

type LogLevel int

const (
	LogError LogLevel = iota
	//LogWarn // ??
	LogInfo
	LogDebug
	LogTrace
)

type Logger struct {
	name, ps       string
	lv             LogLevel
	outputFile     string
	outputWriter   io.Writer
	t0             time.Time
	writeTime      bool
	writeFuncName  bool
	writeFilename  bool
	filterPrefixes map[string]bool
	enabled        bool
	//logger         log.Logger
}

func (l *Logger) LogLevel() LogLevel      { return l.lv }
func (l *Logger) SetLogLevel(lv LogLevel) { l.lv = lv }
func (l *Logger) SetWriteTime(v bool)     { l.writeTime = v }
func (l *Logger) SetWriteFuncName(v bool) { l.writeFuncName = v }
func (l *Logger) SetWriteFilename(v bool) { l.writeFilename = v }
func (l *Logger) SetEnabled(v bool)       { l.enabled = v }
func (l *Logger) Enabled() bool           { return l.enabled }

func NewLogger(name string, lv LogLevel) *Logger {
	l := &Logger{
		name: name,
		//ps: "#%s'%s: ",
		//ps:             "%s.log: %s ",
		//ps:             "%s: %s ",
		lv:             lv,
		t0:             time.Now(),
		writeFilename:  true,
		filterPrefixes: make(map[string]bool, 0),
		enabled:        true,
		//logger:       log.New(os.Stdout, s, log.Lshortfile),
	}
	return l
}

// Add filter prefix.
func (l *Logger) EnableFilterPrefix(prefix string) {
	l.filterPrefixes[prefix] = true
}

// Remove filter prefix.
func (l *Logger) DisableFilterPrefix(prefix string) {
	if ok, _ := l.filterPrefixes[prefix]; ok {
		delete(l.filterPrefixes, prefix)
	}
}

// Clear filter prefixes list.
func (l *Logger) ClearFilterPrefixes() {
	l.filterPrefixes = make(map[string]bool, 0)
}

func (l *Logger) SetOutputFile(s string) {
	l.outputFile = s

	w, err := os.Create(l.outputFile)
	if err != nil {
		panic("SetOutputFile failed; path: " + s)
	}

	l.outputWriter = w
}

func (l *Logger) SetLogLevelStr(s string) {
	l.SetLogLevel(str2loglev(s))
}

func (l *Logger) LogLevelStr() string {
	return loglev2str(l.lv)
}

func (l *Logger) ToggleEnabled() {
	l.enabled = !l.enabled
}

/*
func (l *Logger) _logvals(lv LogLevel, v ...interface{}) {
	pname := loglev2str(lv)
	fmt.Printf(l.ps, l.name, pname)
	fmt.Println(v...)
}
*/

func (l *Logger) logvals(lv LogLevel, format bool, v ...interface{}) {
	if !l.enabled {
		return
	}

	msgTxt := ""
	if !format {
		//msgTxt = fmt.Sprint(v...)
		for _, val := range v {
			msgTxt += fmt.Sprint(val) + " "
		}
		//panic("derp1")
	} else {
		msgTxt = fmt.Sprintf(v[0].(string), v[1:]...)
		//panic("derp2")
	}

	//fmt.Println("msgTxt =", msgTxt)
	//fmt.Printf("msgTxt = %s\n", msgTxt)
	//fmt.Printf("len(v) = %d\n", len(v))

	// Filter prefixes
	for s, enabled := range l.filterPrefixes {
		if !enabled {
			continue
		}
		if strings.HasPrefix(msgTxt, s) {
			return
		}
	}

	/*
		pname := ""
			if lv != LogInfo {
				pname = loglev2str(lv)
			}
	*/
	pname := loglev2str(lv)
	//pname := loglev2strShort(lv)
	msgTime := ""
	_ = msgTime
	if l.writeTime {
		t1 := time.Now()
		dt := t1.Sub(l.t0).Seconds()
		msgTime = fmt.Sprintf("%.2f ", dt)
	}

	msgFuncName := ""
	msgFuncNameFName := "" // Unfiltered?
	msgLoc := ""
	//if l.writeFuncName {
	if true {
		fpcs := make([]uintptr, 1)
		// Skip 2 levels to get the caller
		//n := runtime.Callers(2, fpcs)
		n := runtime.Callers(3, fpcs)
		if n == 0 {
			fmt.Println("MSG: NO CALLER")
		}

		caller := runtime.FuncForPC(fpcs[0] - 1)
		if caller == nil {
			fmt.Println("MSG CALLER WAS NIL")
		}

		//fmt.Println("debug:")
		// Print the file name and line number
		//fmt.Println(caller.FileLine(fpcs[0] - 1))

		// Print the name of the function
		//fmt.Println(caller.Name())

		// Loc
		file, line := caller.FileLine(fpcs[0] - 1)
		msgLoc = fmt.Sprintf("%s:%d", filepath.Base(file), line)

		// FName
		fName := ""
		fnameFull := caller.Name()
		s := strings.Split(fnameFull, ".")
		//if len(s) != 2 && len(s) != 3 {
		if len(s) < 2 {
			panic(fmt.Sprintf("bad lenght: %d", len(s)))
		}
		//fmt.Println("s:", s)

		// Patch fName
		typ := -1
		//_, err := url.Parse(fmt.Sprintf("%s.%s", s[0], s[1]))
		x := s[len(s)-2]
		//fmt.Println("x:", x)
		if strings.HasPrefix(x, "(") && strings.HasSuffix(x, ")") {
			typ = 2
		} else {
			typ = 1
		}

		if typ == 1 {
			fName = s[len(s)-1]
			msgFuncNameFName = fName
			fName = fmt.Sprintf("%s: ", fName)
		} else if typ == 2 {
			//fmt.Println(s)
			_pName := s[len(s)-3]
			_sName := s[len(s)-2]
			_fName := s[len(s)-1]
			// Omit main module
			if _pName == "main" {
				_pName = ""
			}
			// Always omit module
			_pName = ""
			_sName = strings.Replace(_sName, "(", "", -1)
			_sName = strings.Replace(_sName, ")", "", -1)
			_sName = strings.Replace(_sName, "*", "", -1)
			if _pName == "" {
				//fName = fmt.Sprintf("%s.%s", _sName, _fName)
				//fName = fmt.Sprintf("[%s] %s", _sName, _fName)
				fName = fmt.Sprintf("[%s]: ", _sName)
			} else {
				panic("not supported")
				/*
					fName = fmt.Sprintf("%s.%s.%s", _pName, _sName, _fName)
					//fName = fmt.Sprintf("%s.%s.%s", _pName, _sName, _fName)
				*/
			}
			msgFuncNameFName = _fName
			//panic(3)
		} else {
			panic("bad typ")
		}

		msgFuncName = fName

	}
	//panic(2)

	if !l.writeFuncName {
		msgFuncName = ""
	}
	//fmt.Println("x:", msgFuncName)

	if !l.writeFilename {
		msgLoc = ""
	}

	// Filter msgTxt
	msgTxt = strings.Replace(msgTxt, "%F", msgFuncNameFName, -1)

	msg := ""
	if msgLoc != "" {
		msg += fmt.Sprintf("%s: %s%s %s: %s", l.name, msgTime, pname, msgLoc, msgFuncName)
	} else {
		msg += fmt.Sprintf("%s: %s%s %s", l.name, msgTime, pname, msgFuncName)
	}
	msg += msgTxt
	//fmt.Println(msg)
	fmt.Printf("%s\n", msg)

	// Write to file
	if l.outputFile != "" {
		//_, err := fmt.Fprint(l.outputWriter, fmt.Sprintf("%s\n", msg))
		_, err := io.WriteString(l.outputWriter, fmt.Sprintf("%s\n", msg))
		if err != nil {
			panic(err)
		}
	}

	var _ = `
	//s := fmt.Sprintf(l.ps, l.name, pname)
	s := fmt.Sprintf("%s: %s%s%s ", l.name, msgTime, msgFuncName, pname)
	//s += msgTime
	//fmt.Println(v...)
	logger := log.New(os.Stdout, s, log.Lshortfile)
	msg := ""
	//msg := " "

	//if len(pname) > 0 {
	//	msg += pname + " "
	//}
	msg += msgTxt
	//msg += fmt.Sprintf("%-20s %-10s", msgTxt, msgTime)
	//msg = fmt.Sprintf("%-45s%5s", msgTxt, msgTime)
	//fmt.Printf("msg: %q \n", msg)
	//panic(msg)
	logger.Output(3, msg)

	// write to file
	if l.outputFile != "" {
		logger.SetOutput(l.outputWriter)
		logger.Output(3, msg)
	}
	`
}

func (l *Logger) Trc(v ...interface{}) {
	if !(l.lv >= LogTrace) {
		return
	}
	l.logvals(LogTrace, false, v...)
}

func (l *Logger) Dbg(v ...interface{}) {
	if !(l.lv >= LogDebug) {
		return
	}
	l.logvals(LogDebug, false, v...)
}

func (l *Logger) Inf(v ...interface{}) {
	if !(l.lv >= LogInfo) {
		return
	}
	l.logvals(LogInfo, false, v...)
}

func (l *Logger) Err(v ...interface{}) {
	if !(l.lv >= LogError) {
		return
	}
	l.logvals(LogError, false, v...)
}

// XXX dup

func (l *Logger) Trcf(v ...interface{}) {
	if !(l.lv >= LogTrace) {
		return
	}
	l.logvals(LogTrace, true, v...)
}

func (l *Logger) Dbgf(v ...interface{}) {
	if !(l.lv >= LogDebug) {
		return
	}
	l.logvals(LogDebug, true, v...)
}

func (l *Logger) Inff(v ...interface{}) {
	if !(l.lv >= LogInfo) {
		return
	}
	l.logvals(LogInfo, true, v...)
}

func (l *Logger) Errf(v ...interface{}) {
	if !(l.lv >= LogError) {
		return
	}
	l.logvals(LogError, true, v...)
}

var loglev2strMap = map[LogLevel]string{
	LogError: "ERROR",
	LogInfo:  "INFO",
	LogDebug: "DEBUG",
	LogTrace: "TRACE",
}

var loglev2strShortMap = map[LogLevel]string{
	LogError: "ERR",
	LogInfo:  "INF",
	LogDebug: "DBG",
	LogTrace: "TRC",
}

func str2loglev(s string) LogLevel {
	for k, v := range loglev2strMap {
		if v == s {
			return k
		}
	}
	panic("unknow log level: " + s)
}

func loglev2str(lv LogLevel) string {
	if v, ok := loglev2strMap[lv]; ok {
		return v
	}
	panic("unknow log level")
}

func loglev2strShort(lv LogLevel) string {
	if v, ok := loglev2strShortMap[lv]; ok {
		return v
	}
	panic("unknow log level")
}

var _ = `
func getFunc() (string, string, int) {
	// Ask runtime.Callers for up to 5 pcs, including runtime.Callers itself.
	pc := make([]uintptr, 5)
	n := runtime.Callers(0, pc)
	if n == 0 {
		// No pcs available. Stop now.
		// This can happen if the first argument to runtime.Callers is large.
		return "NIL", "NIL", 0
	}

	pc = pc[:n] // pass only valid pcs to runtime.CallersFrames
	frames := runtime.CallersFrames(pc)

	n = 5

	// Loop to get frames.
	// A fixed number of pcs can expand to an indefinite number of Frames.
	for i := 0; i < n; i++ {
		frame, more := frames.Next()
		if i == n-1 {
			return frame.File, frame.Function, frame.Line
		}
		if !more {
			break
		}
	}
	return "NIL", "NIL", 0
}
`
