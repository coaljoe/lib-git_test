package debug

// TODO: Add pf?
// TODO: multiple log files for multi level messages (everything.log, trace.log etc)
// TODO: handling levels my env(?)
// TODO: work with argparse?
// TODO: logic for masking/filtering for modules/files (not, except, and, wildcard etc)

import (
	"fmt"
	"runtime"
	"runtime/debug"
	"strconv"
	"strings"
	//"path/filepath"
	"os"
	"path"

	"github.com/davecgh/go-spew/spew"
)

var Ps = debug.PrintStack

// Overrides other settings if set (forces debug output)
var verbose = false

var pDisabled = false

// -1 = Verbose 0 // Turned off
// 0 = Verbose 0 // Important messages(?)
// 0 = Verbose 0 // Informative messages (log.Inf)
// 1 = Verbose 1 // Debug, less important messages (p, log.Dbg(?))
// 2 = Verbose 2 // Trace, even less important (p, pz, log.Trc(?))
// 3 = Verbose 3 // Everything?
var plLevel = 0

func init() {
	//env := os.Environ()
	haveVerbose := false
	val, ok := os.LookupEnv("VERBOSE")
	if !ok {
		x, ok := os.LookupEnv("V")
		if ok {
			val = x
			haveVerbose = true
		}
	} else {
		haveVerbose = true
	}
	if haveVerbose {
		if strings.ToLower(val) == "true" {
			verbose = true
		} else if strings.ToLower(val) == "false" {
			// Explicit verbose disables p() XXX fixme?
			DisableP()
		} else {

			intVal, err := strconv.Atoi(val)
			if err != nil {
				//panic(fmt.Sprintf("error: can't parse value: %v, error: %v", val, err))
				//panic(err)
			}
			if intVal > 0 {
				verbose = true
			} else {
				// Explicit verbose disables p() XXX fixme?
				DisableP()
			}
		}
	}
}

func DisableP() {
	pDisabled = true
}

func EnableP() {
	pDisabled = false
}

func ToggleP() {
	pDisabled = !pDisabled
}

func Verbose() bool {
	return verbose
}

func P(args ...interface{}) {
	//println("verbose:", verbose)
	//println("pDisabled:", pDisabled)
	if pDisabled && !verbose {
		return
	}

	fileName, fileNameShort := getInfo()
	_ = fileName
	_ = fileNameShort
	//println(fileName)
	//println(fileNameShort)
	//panic(3)

	//fmt.Printf("[")
	//fmt.Println(args...)
	//fmt.Print(args...)
	//fmt.Printf("]\n")
	s := fmt.Sprintln(args...)
	//s = strings.TrimRight(s, "\n")
	//fmt.Printf("[%s]\n", s)
	fmt.Printf("[%s]\n", s[:len(s)-1])
}

func Pv(args ...interface{}) {
	if pDisabled && !verbose {
		return
	}
	fmt.Printf("pv: %#v\n", args...)
}

func Pf(args ...interface{}) {
	if pDisabled && !verbose {
		return
	}
	fmt.Printf("pf: ")
	fmt.Printf(args[0].(string), args[1:]...)
	fmt.Printf("\n")
}

// XXX cannot be disabled
func Pp(args ...interface{}) {
	fmt.Println(args...)
	panic("pp")
}

// XXX cannot be disabled
func Pe(args ...interface{}) {
	fmt.Print("error: ")
	fmt.Println(args...)
	panic("pe")
}

func Pl(level int, args ...interface{}) {
	if pDisabled && !verbose {
		return
	}
	if level <= plLevel {
		P(args...)
	}
}

func Pz(args ...interface{}) {
	if pDisabled && !verbose {
		return
	}
	P("Z", args)
}

func Dump(s interface{}) {
	cs := spew.NewDefaultConfig()
	cs.ContinueOnMethod = true
	cs.MaxDepth = 3
	//cs.MaxDepth = 5
	cs.Dump(s)
	//spew.Dump(s)
}

func DumpDepth(s interface{}, depth int) {
	cs := spew.NewDefaultConfig()
	cs.ContinueOnMethod = true
	cs.MaxDepth = depth
	cs.Dump(s)
	//spew.Dump(s)
}

func Pdump(s interface{}) {
	Dump(s)
	panic("pdump")
}

func getInfo() (string, string) {

	fpcs := make([]uintptr, 1)
	// Skip 2 levels to get the caller
	//n := runtime.Callers(2, fpcs)
	n := runtime.Callers(3, fpcs)
	if n == 0 {
		fmt.Println("MSG: NO CALLER")
	}

	caller := runtime.FuncForPC(fpcs[0] - 1)
	if caller == nil {
		fmt.Println("MSG CALLER WAS NIL")
	}

	//fmt.Println("debug:")
	// Print the file name and line number
	//fmt.Println(caller.FileLine(fpcs[0] - 1))

	// Print the name of the function
	//fmt.Println(caller.Name())

	// Loc
	file, line := caller.FileLine(fpcs[0] - 1)
	_ = file
	_ = line

	fileName := path.Base(file)
	fileNameShort := strings.Split(fileName, ".")[0]

	//println(file, line)
	//println(fileName)
	//println(fileNameShort)
	//panic(2)

	return fileName, fileNameShort
}
