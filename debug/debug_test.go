package debug

import "testing"

func TestDebug(t *testing.T) {
	P("test P")
	Pz("test Pz")

	DisableP()
	P("hidden text")
	EnableP()
}
